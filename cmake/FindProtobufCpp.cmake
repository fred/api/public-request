#
# Locate and configure the Google Protocol Buffers library
#
# Adds the following targets:
#
#  protobuf::libprotobuf - Protobuf library
#  protobuf::libprotobuf-lite - Protobuf lite library
#  protobuf::libprotoc - Protobuf Protoc Library
#  protobuf::protoc - protoc executable
#

# Find the include directory
find_path(PROTOBUF_INCLUDE_DIR google/protobuf/service.h)
mark_as_advanced(PROTOBUF_INCLUDE_DIR)

# Find Protobuf library dependencies
find_package(PkgConfig)

# The Protobuf library
find_library(PROTOBUF_LIBRARY NAMES protobuf)
mark_as_advanced(PROTOBUF_LIBRARY)
if(NOT TARGET protobuf::libprotobuf)
    add_library(protobuf::libprotobuf UNKNOWN IMPORTED)
    pkg_check_modules(PkgConfigProtobuf protobuf)
    set_target_properties(protobuf::libprotobuf PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES ${PROTOBUF_INCLUDE_DIR}
        INTERFACE_LINK_LIBRARIES "${PkgConfigProtobuf_LDFLAGS}"
        IMPORTED_LOCATION ${PROTOBUF_LIBRARY}
    )
endif()

# The Protobuf lite library
find_library(PROTOBUF_LITE_LIBRARY NAMES protobuf-lite)
mark_as_advanced(PROTOBUF_LITE_LIBRARY)
if(NOT TARGET protobuf::libprotobuf-lite)
    add_library(protobuf::libprotobuf-lite UNKNOWN IMPORTED)
    pkg_check_modules(PkgConfigProtobuf protobuf)
    set_target_properties(protobuf::libprotobuf-lite PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES ${PROTOBUF_INCLUDE_DIR}
        INTERFACE_LINK_LIBRARIES "${PkgConfigProtobuf_LDFLAGS}"
        IMPORTED_LOCATION ${PROTOBUF_LITE_LIBRARY}
    )
endif()

# The Protobuf Protoc Library
find_library(PROTOBUF_PROTOC_LIBRARY NAMES protoc)
mark_as_advanced(PROTOBUF_PROTOC_LIBRARY)
if(NOT TARGET protobuf::libprotoc)
    add_library(protobuf::libprotoc UNKNOWN IMPORTED)
    set_target_properties(protobuf::libprotoc PROPERTIES
        INTERFACE_INCLUDE_DIRECTORIES ${PROTOBUF_INCLUDE_DIR}
        INTERFACE_LINK_LIBRARIES protobuf::libprotobuf
        IMPORTED_LOCATION ${PROTOBUF_PROTOC_LIBRARY}
    )
endif()

# Find the protoc Executable
if(NOT TARGET protobuf::protoc)
  find_program(PROTOBUF_PROTOC_EXECUTABLE NAMES protoc)
  mark_as_advanced(PROTOBUF_PROTOC_EXECUTABLE)
  add_executable(protobuf::protoc IMPORTED)
  set_target_properties(protobuf::protoc PROPERTIES
    IMPORTED_LOCATION ${PROTOBUF_PROTOC_EXECUTABLE})
endif()

include(${CMAKE_ROOT}/Modules/FindPackageHandleStandardArgs.cmake)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(ProtobufCpp DEFAULT_MSG
    PROTOBUF_LIBRARY PROTOBUF_INCLUDE_DIR PROTOBUF_PROTOC_EXECUTABLE)

include("${CMAKE_CURRENT_LIST_DIR}/ProtobufGenerateCpp.cmake")
