if(DEFINED COMMON_CMAKE_338CD61DD74736B6B23A1858276DEC76)
    return()
endif()
set(COMMON_CMAKE_338CD61DD74736B6B23A1858276DEC76 1)

set(API_PUBLIC_REQUEST_VERSION "0.2.0")
message(STATUS "API_PUBLIC_REQUEST_VERSION: ${API_PUBLIC_REQUEST_VERSION}")

set(PROTOS_DIR ${CMAKE_CURRENT_LIST_DIR}/..)
set(PROTOBUF_IMPORT_DIRS ${PROTOS_DIR})
set(FRED_PUBLIC_REQUEST_GRPC_FILES
    ${PROTOS_DIR}/fred_api/public_request/service_public_request_info_grpc.proto
    ${PROTOS_DIR}/fred_api/public_request/service_public_request_processing_grpc.proto)

set(FRED_PUBLIC_REQUEST_PROTO_FILES
    ${PROTOS_DIR}/fred_api/public_request/common_types.proto
    ${FRED_PUBLIC_REQUEST_GRPC_FILES})

set(FRED_PUBLIC_REQUEST_GENERATE_INTO ${CMAKE_CURRENT_BINARY_DIR})

set(PROTOBUF_PROTOC_OPTIONS --experimental_allow_proto3_optional)
set(GRPC_PROTOC_OPTIONS --experimental_allow_proto3_optional)
