ChangeLog
=========

.. contents:: Releases
   :backlinks: none
   :local:

Unreleased
----------

0.2.0 (2023-02-22)
------------------

API breaking changes:
* Rename ``ObjectType`` to ``RegistryObjectType``.
* Rename ``ObjectId`` to ``RegistryObjectReference``.
* Rename ``PublicRequest.object_id`` to ``PublicRequest.registry_object_reference``.
* Rename ``ListRequest.object_id`` to ``ListRequest.registry_object_reference``.


0.1.0 (2023-01-31)
------------------

Initial version

